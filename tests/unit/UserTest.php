<?php

class UserTest extends PHPUnit_Framework_TestCase {
  /** @test */
  public function first_test() {
    $user = new App\Models\User;

    $this->assertEquals($user->getSum(), 9);
  }

  /** @test */
  public function second_test() {
    $user = new App\Models\User;

    $this->assertEquals($user->getProduct(), 12);
  }

  /** @test */
  public function third_test() {
    $this->assertEquals(0, 0);
  }
}
